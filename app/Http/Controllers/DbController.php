<?php

namespace App\Http\Controllers;

use App\Tutor;
use App\EducationalInfo;
use App\PersonalInfo;
use App\TutoringInfo;
use App\InterestedList;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class DbController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $intlist = DB::table('interested_lists')
            ->whereNull('deleted_at')
            ->orderBy('id','desc')
            ->paginate(5);

        return view('pages.interested-list', ['intlists' => $intlist]);
    }

    public function search(Request $request)
    {
        $res = $request->input('res');

        $intlists = DB::table('interested_lists')
            ->where('username', 'like', '%'.$res.'%')
            ->orWhere('studentname', 'like', '%'.$res.'%')
            ->orWhere('email', 'like','%'.$res.'%')
            ->orWhere('id', 'like', $res.'%')
            ->orWhere('post_id', 'like', $res.'%')
            ->orWhere('student_phone', 'like', '%'.$res.'%')
            ->orWhere('teacher_phone', 'like', '%'.$res.'%')
            ->orWhere('salary', 'like', '%'.$res.'%')
            ->whereNull('deleted_at')
            ->orderBy('id')
            ->get();

        return view('intlist.search-result', compact('intlists','res'));
    }

    public function store(Request $request)
    {
        InterestedList::index($request->all());  //method 1 to return full table
    }

    public function show($id)
    {
        $tutor = EducationalInfo::findOrFail($id);
        $personalinfo = PersonalInfo::findOrFail($id);
        $user = User::findOrFail($id);
        $intlist = InterestedList::findOrFail($id);
        return view('dashboard.show', compact('tutor','personalinfo','user','intlist'));
    }

    public function edit($id)
    {
        $intlist = InterestedList::findOrFail($id);
        return view('dashboard.edit', compact('intlist'));
    }
    public function update(Request $request, $id)
    {
        $intlist = InterestedList::findOrFail($id);
        if(!isset($request->active))
            $intlist->update(array_merge($request->all(),['active'=>0]));
        if(isset($request->active))
            $intlist->update(array_merge($request->all(),['active'=>1]));
        return redirect('/db');

    }

    public function destroy($id){
        InterestedList::destroy($id);
        return redirect('/db');
    }
}