<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class InterestedList extends Model
{

    protected $table = "interested_lists";
    /*public $incrementing = false;
    protected  $primaryKey ='email';*/
    use SoftDeletes;
    //protected $fillable = ['username','active','phone','address','institute'];
    protected $dates = ['deleted_at'];

    /*public function setActiveAttribute($value){
        $this->attributes['active'] = ($value);
    }
    public function setUsernameAttribute($value){
        $this->attributes['username'] = ($value);
    }
    public function setPhoneAttribute($value){
        $this->attributes['phone'] = ($value);
    }
    public function setAddressAttribute($value){
        $this->attributes['address'] = ($value);
    }
    public function setInstituteAttribute($value){
        $this->attributes['institute'] = ($value);
    }*/
}

