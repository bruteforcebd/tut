/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.14-log : Database - tuitioncenter
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `educationalinfos` */

DROP TABLE IF EXISTS `educationalinfos`;

CREATE TABLE `educationalinfos` (
  `id` int(63) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `id_card` varchar(400) DEFAULT 'http://tuition.tutorcenter-bd.com/images/id/id_card.png',
  `ssc_year_of_passing` varchar(50) DEFAULT '',
  `ssc_group` varchar(50) DEFAULT '',
  `ssc_gpa` varchar(50) DEFAULT '',
  `hsc_year_of_passing` varchar(50) DEFAULT '',
  `hsc_group` varchar(50) DEFAULT '',
  `hsc_gpa` varchar(50) DEFAULT '',
  `under_grad_year_of_passing` varchar(50) DEFAULT '',
  `under_grad_major` varchar(50) DEFAULT '',
  `under_grad_cgpa` varchar(50) DEFAULT '',
  `grad_year_of_passing` varchar(50) DEFAULT '',
  `grad_major` varchar(50) DEFAULT '',
  `grad_cgpa` varchar(50) DEFAULT '',
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`email`),
  KEY `id` (`id`),
  CONSTRAINT `educationalinfos_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `interested_lists` */

DROP TABLE IF EXISTS `interested_lists`;

CREATE TABLE `interested_lists` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `post_id` bigint(25) NOT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  `studentname` varchar(100) DEFAULT NULL,
  `student_phone` varchar(20) DEFAULT NULL,
  `teacher_phone` varchar(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `date_time` varchar(63) DEFAULT NULL,
  `date_to_start` varchar(63) DEFAULT NULL,
  `salary` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`,`email`,`post_id`),
  KEY `interested_id` (`id`),
  KEY `email` (`email`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `interested_lists_ibfk_2` FOREIGN KEY (`email`) REFERENCES `users` (`email`),
  CONSTRAINT `interested_lists_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `jobfeeds` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `jobfeeds` */

DROP TABLE IF EXISTS `jobfeeds`;

CREATE TABLE `jobfeeds` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `date_to_start` varchar(20) NOT NULL,
  `num_of_students` tinyint(4) NOT NULL,
  `preferred_medium` varchar(63) NOT NULL,
  `days_in_week` tinyint(1) NOT NULL,
  `preferred_teacher_gender` varchar(15) DEFAULT NULL,
  `content` varchar(2048) DEFAULT NULL,
  `salary` int(6) NOT NULL,
  `student_class` varchar(63) NOT NULL,
  `image` varchar(400) DEFAULT NULL,
  `date_time` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `address` varchar(255) NOT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `jobfeeds_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `personalinfos` */

DROP TABLE IF EXISTS `personalinfos`;

CREATE TABLE `personalinfos` (
  `id` int(63) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `additional_number` varchar(11) DEFAULT '',
  `detail_address` varchar(63) DEFAULT '',
  `nid_passport_no` varchar(15) DEFAULT '',
  `fb_id` varchar(50) DEFAULT '',
  `linkedin_id` varchar(100) DEFAULT '',
  `father_name` varchar(50) DEFAULT '',
  `mother_name` varchar(50) DEFAULT '',
  `father_number` varchar(11) DEFAULT '',
  `mother_number` varchar(11) DEFAULT '',
  `ref_person_name` varchar(50) DEFAULT '',
  `ref_person_number` varchar(11) DEFAULT '',
  `ref_person_relation` varchar(20) DEFAULT '',
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`email`),
  KEY `id` (`id`),
  CONSTRAINT `personalinfos_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `id_card` varchar(250) DEFAULT 'http://tuition.tutorcenter-bd.com/images/id/id_card.png',
  `ref_person_name` varchar(50) DEFAULT '',
  `ref_phone_no` varchar(50) DEFAULT '',
  `ref_person_relation` varchar(50) DEFAULT '',
  `institute_name` varchar(50) DEFAULT '',
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`email`),
  KEY `email` (`email`),
  KEY `id` (`id`),
  CONSTRAINT `students_ibfk_2` FOREIGN KEY (`email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Table structure for table `tutoringinfos` */

DROP TABLE IF EXISTS `tutoringinfos`;

CREATE TABLE `tutoringinfos` (
  `id` int(63) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `referred_person_name` varchar(63) DEFAULT '',
  `medium_catagory` varchar(50) DEFAULT '',
  `class_course` varchar(200) DEFAULT '',
  `subject` varchar(200) DEFAULT '',
  `service_location` varchar(50) DEFAULT '',
  `have_experience` int(1) DEFAULT '0',
  `total_experience` varchar(127) DEFAULT '',
  `experience_details` varchar(500) DEFAULT '',
  `available_days` varchar(200) DEFAULT '',
  `from_time` varchar(15) DEFAULT '',
  `to_time` varchar(15) DEFAULT '',
  `expected_salary` int(6) DEFAULT NULL,
  `teaching_method` varchar(500) DEFAULT '',
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`email`),
  KEY `email` (`email`),
  KEY `id` (`id`),
  CONSTRAINT `tutoringinfos_ibfk_2` FOREIGN KEY (`email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(63) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `region` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `gender` varchar(7) DEFAULT NULL,
  `profile_pic` varchar(1023) DEFAULT 'http://tuition.tutorcenter-bd.com/images/uploads/profile_pic.png',
  `active` tinyint(1) DEFAULT '0',
  `remember_token` varchar(500) NOT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`email`,`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/* Trigger structure for table `users` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `afterUserInsert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `afterUserInsert` AFTER INSERT ON `users` FOR EACH ROW BEGIN
     
     IF new.user_type='student'  THEN 
      INSERT INTO tutorcentbd_troubles_ged_v1.students(id,email)VALUES(new.id, new.email);
     END IF;
     IF new.user_type='teacher'  THEN 
      INSERT INTO tutorcentbd_troubles_ged_v1.educationalinfos(id,email)VALUES(new.id, new.email);
      INSERT INTO tutorcentbd_troubles_ged_v1.personalinfos(id,email)VALUES(new.id, new.email);
      INSERT INTO tutorcentbd_troubles_ged_v1.tutoringinfos(id,email)VALUES(new.id, new.email);
     END IF;
    END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
