<?php

namespace App\Http\Controllers;

use App\Student;
use App\Students;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $students = DB::table('students')
            ->join('users', 'users.email', '=', 'students.email')
            ->where('users.user_type', '=', 'student')
            ->where('user_type', '=', 'student')
            ->whereNull('users.deleted_at')
            ->orderBy('users.id','desc')
            ->paginate(5);

        return view('pages.student', compact('students'));
    }

    public function search(Request $request)
    {

        //$res = Input::get('res', 'default res');
        //$input = Input::get();
        $res = $request->input('res');
        //return view('writerregistration', ['profession' => $profession]);

        $students = DB::table('students')
            ->join('users', 'users.email', '=', 'students.email')
            ->where('users.user_type', '=', 'student')
            ->where('users.username', 'like', '%'.$res.'%')
            ->orWhere('users.email', 'like','%'.$res.'%')
            ->orWhere('users.id', 'like', $res.'%')
            ->orWhere('users.phone', 'like', '%'.$res.'%')
            ->orWhere('users.institute', 'like', '%'.$res.'%')
            ->whereNull('users.deleted_at')
            ->orderBy('users.id')
            ->get();

        return view('students.search-results', compact('students','res'));
    }

    public function store(Request $request)
    {
        Student::index($request->all());  //method 1 to return full table
    }

    public function show($id)
    {
        $student = Students::findOrFail($id);
        $user = User::findOrFail($id);
        return view('students.show', compact('student','user'));
    }

    public function edit($id)
    {
        $student = Student::findOrFail($id);
        return view('students.edit', compact('student'));
    }
    public function update(Request $request, $id)
    {
        $student = Student::findOrFail($id);
        if(!isset($request->active))
            $student->update(array_merge($request->all(),['active'=>0]));
        if(isset($request->active))
            $student->update(array_merge($request->all(),['active'=>1]));
            return redirect('/student');

    }
    public function destroy($id){
        Student::destroy($id);
        Students::destroy($id);
        return redirect('/student');
    }
}