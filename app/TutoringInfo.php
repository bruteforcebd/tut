<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TutoringInfo extends Model
{
    /*protected $primaryKey = "email";*/
    use SoftDeletes;

    protected $table = "tutoringinfos";
    protected $dates = ['deleted_at'];



}

