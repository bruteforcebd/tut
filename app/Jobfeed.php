<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Jobfeed extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = "jobfeeds";

    protected $fillable = ['username','date_to_start','active','num_of_students','preferred_medium','days_in_week','preferred_teacher_gender','content','salary','student_class','date_time','title','subject','address'];

    //protected $primaryKey = 'post_id';

    public function setActiveAttribute($value){
        $this->attributes['active'] = ($value);
    }
    public function setUsernameAttribute($value){
        $this->attributes['username'] = ($value);
    }
    public function setNumOfStudentsAttribute($value){
        $this->attributes['num_of_students'] = ($value);
    }
    public function setPreferredMediumAttribute($value){
        $this->attributes['preferred_medium'] = ($value);
    }
    public function setDaysInWeekAttribute($value){
        $this->attributes['days_in_week'] = ($value);
    }
    public function setPreferredTeacherGenderAttribute($value){
        $this->attributes['preferred_teacher_gender'] = ($value);
    }
    public function setContentAttribute($value){
        $this->attributes['content'] = ($value);
    }
    public function setSalaryAttribute($value){
        $this->attributes['salary'] = ($value);
    }
    public function setStudentClassAttribute($value){
        $this->attributes['student_class'] = ($value);
    }
    public function setDateTimeAttribute($value){
        $this->attributes['date_time'] = ($value);
    }
    public function setTitleAttribute($value){
        $this->attributes['title'] = ($value);
    }
    public function setSubjectAttribute($value){
        $this->attributes['subject'] = ($value);
    }
    public function setAddressAttribute($value){
        $this->attributes['address'] = ($value);
    }
    public function setDateToStartAttribute($value){
        $this->attributes['date_to_start'] = ($value);
    }
}

