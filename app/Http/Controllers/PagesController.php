<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function tutor(){
        return view('pages.tutor');
    }
    public function student(){
        return view('pages.student');
    }
    public function post(){
        return view('pages.post');
    }

}
